package dadd.tugas;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.FlowLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JButton btnKirim;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setTitle("Tugas DADD - Socket");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 50);
		setMinimumSize(new Dimension(400, 100));
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblInputMessage = new JLabel("Masukkan Perintah");
		contentPane.add(lblInputMessage);
		
		textField = new JTextField();
		contentPane.add(textField);
		textField.setColumns(20);
		
		btnKirim = new JButton("Kirim");
		btnKirim.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				String text = textField.getText();
				try {
					Kontrol.connectAndSend(text);
				} catch(IOException e) {
					JOptionPane.showMessageDialog(GUI.this, "Error: " + e.getMessage());
				}
			}
		});
		contentPane.add(btnKirim);
	}

}
