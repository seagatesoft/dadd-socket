package dadd.tugas;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.*;


public class SlideShow extends JFrame{
    JLabel pic;
    Timer tm;
    int x = 0;
    //Images Path In Array
    /*String[] list = {
                      "C:/java/satu.jpg",//0
                      "C:/java/dua.jpg",//1
                      "C:/java/tiga.jpg",//2
                      "C:/java/empat.jpg"//6
                    };*/
    
    String[] list;
    
    public SlideShow(){
        super("Java SlideShow");
        pic = new JLabel();
        pic.setBounds(40, 30, 700, 300);

        File folder = new File(".");
        File[] listOfFiles = folder.listFiles();
        list = new String[listOfFiles.length];
        
        for (int i = 0; i < listOfFiles.length; i++) {
          if (listOfFiles[i].isFile()) {
            //System.out.println("File " + listOfFiles[i].getName());
            list[i] = "." + File.separator + listOfFiles[i].getName();
          } /*else if (listOfFiles[i].isDirectory()) {
            System.out.println("Directory " + listOfFiles[i].getName());
          }*/
        }
        
        //Call The Function SetImageSize
        SetImageSize(list.length -1);
        
       //set a timer
        tm = new Timer(2000,new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                SetImageSize(x);
                x += 1;
                if(x >= list.length )
                    x = 0; 
            }
        });
        add(pic);
        tm.start();
        setLayout(null);
        setSize(800, 400);
        getContentPane().setBackground(Color.decode("#bdb67b"));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
    //create a function to resize the image 
    public void SetImageSize(int i){
        ImageIcon icon = new ImageIcon(list[i]);
        Image img = icon.getImage();
        Image newImg = img.getScaledInstance(pic.getWidth(), pic.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon newImc = new ImageIcon(newImg);
        pic.setIcon(newImc);
    }

public static void main(String[] args){ 
      
    new SlideShow();
}
}

