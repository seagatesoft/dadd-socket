package dadd.tugas;

import java.awt.Color;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.*;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.*;

/**
 * A TCP server that runs on port 9090.  When a client connects, it
 * sends the client the current date and time, then closes the
 * connection with that client.  Arguably just about the simplest
 * server you can write.
 */
public class Displayer {

	private static JFrame aha;
    
	public static void receiveFile(String ipAddress,int portNo,String fileLocation) throws IOException
	{

		int bytesRead=0;
		int current = 0;
		FileOutputStream fileOutputStream = null;
		BufferedOutputStream bufferedOutputStream = null;
		Socket socket = null;
		try {

			//creating connection.
			socket = new Socket(ipAddress,portNo);
			System.out.println("connected.");
			
			// receive file
			byte [] byteArray  = new byte [6022386];					//I have hard coded size of byteArray, you can send file size from socket before creating this.
			System.out.println("Please wait downloading file");
			
			//reading file from socket
			InputStream inputStream = socket.getInputStream();
			fileOutputStream = new FileOutputStream(fileLocation);
			bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
			bytesRead = inputStream.read(byteArray,0,byteArray.length);					//copying file from socket to byteArray

			current = bytesRead;
			do {
				bytesRead =inputStream.read(byteArray, current, (byteArray.length-current));
				if(bytesRead >= 0) current += bytesRead;
			} while(bytesRead > -1);
			bufferedOutputStream.write(byteArray, 0 , current);							//writing byteArray to file
			bufferedOutputStream.flush();												//flushing buffers
			
			System.out.println("File " + fileLocation  + " downloaded ( size: " + current + " bytes read)");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if (fileOutputStream != null) fileOutputStream.close();
			if (bufferedOutputStream != null) bufferedOutputStream.close();
			if (socket != null) socket.close();
		}
	}
	
    /**
     * Runs the server.
     * @throws ClassNotFoundException 
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ServerSocket listener = new ServerSocket(9090);
        System.out.println("Server start listening ...");
        String[] terima;
        File f = null;
        
        try {
            while (true) {
                Socket socket = listener.accept();
                try {
                    //read from socket to ObjectInputStream object
                    ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                    //convert ObjectInputStream object to String
                    String message = (String) ois.readObject();
                    System.out.println("Message Received: " + message);
                    
                    if (message.equals("start")) {
	                	aha = new SlideShow();
	                	aha.setVisible(true);
                    } else if (message.equals("end")) {
                    	aha.dispose();
                    } else {
                    	terima = message.split("#");
                    	 
                    	 if (terima[0].equals("send")) {
	                    	try {
	                    	    //thread to sleep for the specified number of milliseconds
	                    	    Thread.sleep(2000);
	                    	    
	                    	    
	                    	    Path p = Paths.get(terima[1]);
	                    	    String file = p.getFileName().toString();
	                    	    System.out.println(terima[1]);
	                    	    
	                    	    Displayer.receiveFile(terima[2],9091,"." + File.separator + file );
	                    	} catch ( java.lang.InterruptedException ie) {
	                    	    System.out.println(ie);
	                    	}
                    	} else if (terima[0].equals("delete")) {
                    		f = new File("." + File.separator + terima[1]);
                    		f.delete();
                    	}
                    }
                    
                    /*BufferedReader input =
                    new BufferedReader(new InputStreamReader(socket.getInputStream()));
		            String answer = input.readLine();
		            JOptionPane.showMessageDialog(null, answer);
                    
                    PrintWriter out =
                        new PrintWriter(socket.getOutputStream(), true);
                    out.println(new Date().toString());*/
                } finally {
                    socket.close();
                }
            }
        }
        finally {
            listener.close();
        }
    }
}
