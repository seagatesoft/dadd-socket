package dadd.tugas;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.InetAddress;
import javax.swing.JOptionPane;

/**
 * Trivial client for the date server.
 */
public class Kontrol {
	
    /**
     * Runs the client as an application.  First it displays a dialog
     * box asking for the IP address or hostname of a host running
     * the date server, then connects to it and displays the date that
     * it serves.
     */
    public static void connectAndSend(String teks) throws IOException {
    	String[] kirim = teks.split("#");
    	
        Socket s = new Socket(kirim[0], 9090);
        
        teks = "";
        for (int i=1;i<kirim.length;i++) {
        	if (!teks.equals("")) teks += "#";
        	teks += kirim[i] ;
        }
        
        //get ip address of this comp
        InetAddress iAddress = InetAddress.getLocalHost();
        String currentIp = iAddress.getHostAddress();
        
        if (kirim[1].equals("send")) teks += "#" + currentIp;
        
        ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());
        oos.writeObject(teks);
        
        if (!teks.equals("start") && !teks.equals("end")) {
        	kirim = teks.split("#");

        	if (kirim[0].equals("send")) {
	        	Kontrol.send(9091, kirim[1]);
        	}
        } 
        
        /*BufferedReader input =
        new BufferedReader(new InputStreamReader(s.getInputStream()));
    	String answer = input.readLine();
        
        PrintWriter out = new PrintWriter(s.getOutputStream(), true);
        out.println("Alloha");*/
        
        //JOptionPane.showMessageDialog(null, answer);
    }
    
    /**
	 * this function actually transfers file
	 * @param portNo
	 * @param fileLocation
	 * @return
	 * @throws IOException 
	 */
	public static  void send(int portNo,String fileLocation) throws IOException
	{

		FileInputStream fileInputStream = null;
		BufferedInputStream bufferedInputStream = null;

		OutputStream outputStream = null;
		ServerSocket serverSocket = null;
		Socket socket = null;

		//creating connection between sender and receiver
		try {
			serverSocket = new ServerSocket(portNo);
			System.out.println("Waiting for receiver...");
				try {
						socket = serverSocket.accept();
						System.out.println("Accepted connection : " + socket);
						//connection established successfully
	
						//creating object to send file
						File file = new File (fileLocation);
						byte [] byteArray  = new byte [(int)file.length()];
						fileInputStream = new FileInputStream(file);
						bufferedInputStream = new BufferedInputStream(fileInputStream);
						bufferedInputStream.read(byteArray,0,byteArray.length); // copied file into byteArray
	
						//sending file through socket
						outputStream = socket.getOutputStream();
						System.out.println("Sending " + fileLocation + "( size: " + byteArray.length + " bytes)");
						outputStream.write(byteArray,0,byteArray.length);			//copying byteArray to socket
						outputStream.flush();										//flushing socket
						System.out.println("Done.");								//file has been sent
					}
					finally {
						if (bufferedInputStream != null) bufferedInputStream.close();
						if (outputStream != null) bufferedInputStream.close();
						if (socket!=null) socket.close();
					}		
			} catch (IOException e) {
				
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally {
				if (serverSocket != null) serverSocket.close();
			}
	}
}